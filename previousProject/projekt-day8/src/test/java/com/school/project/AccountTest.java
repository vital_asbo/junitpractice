package com.school.project;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void newlyCreatedAccountShouldNotBeActive(){
        // given
        Account account = new Account();

        //then
        assertFalse(account.isActive(),
                "Check if new account is not active");

    }


    @Test
    void newlyCreatedAccountShouldBeActiveAfterActivation(){
        //given
        Account account = new Account();

        //when
        account.activate();

        //then
        assertTrue(account.isActive(),
                "Check if new account is not active");

    }

    @Test
    void newlyCreatedAccountShouldHaveDefaultDeliveryAddressEqualToNull(){

        //1
//        //given
//        Account account = new Account();
//        //when
//        Address address = account.getDefaultDeliverAddress();
//        //then
//        assertNull(address);

        // 2
        //given
        Account account1 = new Account();

        //then
        assertNull(account1.getDefaultDeliverAddress());



    }

    @Test
    void deliveryAddressShouldNotBeNullAfterBeingSetInAccount(){

        // 2
        //given
        Account account1 = new Account();

        //when
        account1.setDefaultDeliverAddress(new Address("Szkolna", "100a"));

        //then
        assertNotNull(account1.getDefaultDeliverAddress());

    }




}
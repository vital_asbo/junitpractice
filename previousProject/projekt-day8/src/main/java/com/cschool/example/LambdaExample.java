package com.cschool.example;

public class LambdaExample {


    public static void main(String[] args) {

        SumImpl sumImpl = new SumImpl();
//        com.cschool.example.Sum sumImpl2 = new com.cschool.example.SumImpl();

        Sum sumInstance = (x, y) -> x - y + 10;
//
        Sum sumInstance10 = ( int a, int b) -> a + 10;

        System.out.println(sumInstance10.calculate(3,2));  //
        System.out.println(sumInstance.calculate(2,3));  //

        Sum sumInstance2 = new Sum() {
            @Override
            public int calculate(int a, int b) {
                return a + b +10;
            }
        };

        Sum sumInstance3 = new Sum() {
            @Override
            public int calculate(int a, int b) {
                return a + b +10;
            }

        };


        System.out.println(sumImpl.calculate(2,6)); // 8
        System.out.println(sumInstance.calculate(2,6)); //  11
        System.out.println(sumInstance2.calculate(2,6)); // 18












    }





}

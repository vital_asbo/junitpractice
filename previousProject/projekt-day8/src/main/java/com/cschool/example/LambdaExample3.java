package com.cschool.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class LambdaExample3 {

    public static void main(String[] args) {


        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        integers.forEach(element -> {
            if (element % 2 == 0) {
//                System.out.println(element);
            }
        });

//        System.out.println();

        List<String> names = new ArrayList<>();
        names.add("Jack");
        names.add("John");
        names.add("Jasmine");

        List<String> modifiedNames = new ArrayList<>();

        Random random = new Random();
        for(String str : names){

            String modifiedName = modifyMethod(str, n -> n+ random.nextInt(11));
//            modifiedNames.add(str + " " + random.nextInt(11));
            modifiedNames.add(modifiedName);

        }
        System.out.println(modifiedNames);

//                                  String modify(String str);
        System.out.println(modifyMethod("DOM", (String x) -> x+ "5"  ));

        //@
        Modifier modifierA = (String x) -> x+ "10";
        System.out.println(modifierA.modify("Pies"));
        System.out.println(modifyMethod("DOM", modifierA));
        // @
    }

    private static String modifyMethod(String str, Modifier modifierB){
        return modifierB.modify(str);
    }







}
